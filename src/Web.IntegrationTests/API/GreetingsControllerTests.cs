using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using NUnit.Framework;

namespace ApiExample.Web.UnitTests.API
{
    [TestFixture]
    public class GreetingsControllerTests
    {
        private readonly string ApiUri = "Greetings/Ankita";
        private WebApplicationFactory<Startup> applicationFactory;
        private HttpClient httpClient;

        [OneTimeSetUp]
        public void SetUp()
        {
            applicationFactory = new WebApplicationFactory<Startup>();
            httpClient = applicationFactory.CreateClient();
        }

        [Test]
        public async Task Get_ReturnsCorrectContentType()
        {
            var result = await httpClient.GetAsync(ApiUri);

            Assert.AreEqual("application/json; charset=utf-8",
                result.Content.Headers.ContentType.ToString());
        }

        [Test]
        public async Task Get_ReturnsMessage()
        {
            var httpResponse = await httpClient.GetAsync(ApiUri);
            var json = await httpResponse.Content.ReadAsStringAsync();
            dynamic responseContent = JsonConvert.DeserializeObject(json);

            string message = responseContent.message;
            Assert.That(message, Is.EqualTo("Welcome to BBC Studios Ankita"));
        }

        [Test]
        public async Task Get_ReturnsSuccessStatusCode()
        {
            var result = await httpClient.GetAsync(ApiUri);

            result.EnsureSuccessStatusCode();
        }
    }
}