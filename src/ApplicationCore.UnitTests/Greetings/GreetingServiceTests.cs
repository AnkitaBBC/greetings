using ApiExample.ApplicationCore.Greetings;
using NUnit.Framework;

namespace ApiExample.ApplicationCore.UnitTests.Greetings
{
    public class GreetingServiceTests
    {
        private GreetingService sut;

        [SetUp]
        public void Setup()
        {
            sut = new GreetingService();
        }

        [Test]
        public void GetMessage_ShouldReturnMessage()
        {
            var expectedMessage = "Welcome to BBC Studios Ankita";
            var name = "Ankita";
            var result = sut.GetMessage(name);

            Assert.AreEqual(expectedMessage, result);
        }
    }
}