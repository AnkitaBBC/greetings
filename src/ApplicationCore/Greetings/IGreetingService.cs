﻿namespace ApiExample.ApplicationCore.Services
{
    public interface IGreetingService
    {
        string GetMessage(string name);
 		string DoSomething(string name);
 	  	string GetName(string name);
        string DoNothing(string name);
    }
}