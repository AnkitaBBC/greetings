﻿using ApiExample.ApplicationCore.Services;

namespace ApiExample.ApplicationCore.Greetings
{
    public class GreetingService : IGreetingService
    {
        public string GetMessage(string name)
        {
            return $"Welcome to BBC Studios " + name;
        }

        public string GetName(string name)
        {
            return $"Welcome to BBC Studios " + name;
        }
        public string DoSomething(string name)
        {
            return $"Welcome to BBC Studios " + name;
        }

        public string DoNothing(string name)
        {
            return $"Welcome to BBC Studios " + name;
        }
    }
}