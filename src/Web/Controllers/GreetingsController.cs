﻿using ApiExample.ApplicationCore.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ApiExample.Web.Controllers
{
    [ApiController]
    public class GreetingsController : ControllerBase
    {
        private readonly IGreetingService greetingService;

        public GreetingsController(IGreetingService greetingService)
        {
            this.greetingService = greetingService;
        }

        [Route("Greetings/{name}")]
        public ActionResult<string> Greetings(string name)
        {
            var greetingMessage = greetingService.GetMessage(name);

            return Ok(new { Message = greetingMessage });
        }

        public ActionResult<string> DoSomething(string name)
        {
            var greetingMessage = greetingService.GetMessage(name);

            return Ok(new { Message = greetingMessage });
        }
    }
}