<# Note: Requires Powershell 5 or above, or PowerShellCore (pwsh) 

# What does this module do? Provides functions that do the following

	* Cleans the directory ..\dist\ for your build output to be placed into
	* Restores nuget dependencies with the Restore command
	* Compiles with msbuild the sln file in the same directory as this script using the configured version of msbuild and sensible defaults
	* Nuget packs any projects containing a nuspec file
	* generates zips and nupkgs for deployment with version numbers
	* Executes any tests identified based on naming convention
	* Logs build and tests outputs to files for you
	* detectes and throws errors if Build() or Test() fail and fails teamcity builds for you
	* Reports progress to the console and teamcity nicely
	* generate swagger files during the build

# How to use this module to create a build script?

See https://bitbucket.org/bbcworldwide/bbc-studios-common-buildscript for docs
See example-build-dotnetcore.ps1 and example-build-net4.ps1 adjacent to this file for example build.ps1 files
#> 

Set-StrictMode -Version latest
$OutputDirectory = if((Test-Path variable:global:OutputDirectory) -and $global:OutputDirectory) { $global:OutputDirectory } else { "..\dist" }
$InfoMessageColour = "Magenta"
 
<#
.SYNOPSIS
If this module has been loaded dynamically this function 
will get the folder of powershell script that loaded the module, 
usually this is the folder containing build.ps1
#>
function Get-ScriptDirectory {
	$Invocation = (Get-Variable MyInvocation -Scope 1).Value;
	if ($Invocation.PSScriptRoot) {
		$Invocation.PSScriptRoot;
	}
	elseif ($Invocation.MyCommand.Path) {
		Split-Path $Invocation.MyCommand.Path
	}
	else {
		$Invocation.InvocationName.Substring(0, $Invocation.InvocationName.LastIndexOf("\"));
	}
}

Function Invoke-BuildTask (
	[string] $Name,
	[bool] $Skip = $false,
	[int] $Retries = 0,
	[int] $SecondsBetweenRetries = 0,
	[scriptblock] $Command) { 
	$taskName = $Name.Replace("'", "|'").Trim();
	$taskStopWatch = [Diagnostics.Stopwatch]::StartNew()
	$success = $false
	$result = $null
	$failedSoFar = ([array] @($buildTasks | Where-Object { -not $_.Succeeded -and -not $_.Skipped })).Length
	if ($failedSoFar -gt 0) { $Skip = $true; }

	if ($IsTeamcity) { Write-Host "##teamcity[blockOpened name='TASK: $taskName']" }
	else { Write-Host "TASK: '$taskName' $(if($skip) { "SKIPPED" } else { "Started" })" -ForegroundColor "Blue" }

	try {
		# Only run the task if all other preceeding have succeeded
		if (-not($Skip)) {

			# Run the task - will fail the task if throws or return $false otherwise is a success
			# optional retry feature to catch transient errors (e.g. npm audit is flaky)
			$result = Invoke-Retry -Retries:$Retries -SecondsDelay:$SecondsBetweenRetries -Cmd { 
				& $command
			}

			if ($result -ne $false) {
				$success = $true
			}
		}
	}
	catch {
		$success = $false
		Write-Error $PSItem
	}
	finally {
		if ($IsTeamcity) { Write-Host "##teamcity[blockClosed name='TASK: $taskName']" }
		$_ = $taskStopWatch.Stop()
		$duration = $taskStopWatch.Elapsed.TotalSeconds

		# Recored the task result and timings in the task log
		$_ = $buildTasks.Add([PSCustomObject]@{
				BuildTask  = $Name
				Skipped    = $Skip
				Succeeded  = $success
				Duration   = [math]::Round($duration, 1)
				Percentage = ""
			})
		 			
		if (-not($Skip)) {
			$duration = Get-PrettyDuration $taskStopWatch.Elapsed
			Write-Host "`TASK: '$taskName' $(if($success){ "succeeded" } else { "FAILED" }) in $duration`n" -ForegroundColor "Blue"
			if ($IsTeamcity -and -not $success ) { Write-Host "##teamcity[buildProblem description='Task |'$taskName|' failed']" }
		}
	}
	return $result
}

Function Get-BuildTasksSummary {
	[array] $failed = @($buildTasks | Where-Object { -not $_.Succeeded -and -not $_.Skipped })
	[array] $succeeded = @($buildTasks | Where-Object { $_.Succeeded })
	[array] $skipped = @($buildTasks | Where-Object { $_.Skipped -and -not $_.Succeeded })
	[bool] $allSuccessful = $succeeded.Count -gt 0 -and $failed.Count -eq 0
	$firstFailure = if ($failed.Count -gt 0) { $failed[0].BuildTask } else { $null }

	$totalDurationInSecs = if ($buildTasks) { ($buildTasks | Select-Object Duration | Measure-Object -Property Duration -Sum).Sum } else { 1 }

	foreach ($task in $buildTasks) {
		$task.Percentage = ($task.Duration / $totalDurationInSecs).ToString("P0")
	}

	return [PSCustomObject]@{
		TasksCount    = $buildTasks.Count
		TasksFailed   = $failed.Count
		TasksSkipped  = $skipped.Count
		TasksSucceded = $succeeded.Count
		Succeeded     = $allSuccessful
		FirstFailure  = $firstFailure
	}
}

<#
	.SYNOPSIS
	Execute a script block more than once after a sleep if it fails. Useful for catching and retrying transient failures
	
	.EXAMPLE
	Invoke-Retry -Retries 3 -SecondsDelay 1 -Cmd {
		if(((Get-Random -Maximum 2) % 2) -eq 0) { throw "random transient error" }
		Write-Output "hello"
	}
	#>
Function Invoke-Retry {
	param(
		[Parameter(Position = 0, Mandatory = $true)] [scriptblock]$Cmd,
		[Parameter(Mandatory = $false)] [int] $Retries = 3,
		[Parameter(Mandatory = $false)] [int] $SecondsDelay = 1
	)
	$retrycount = 0

	while ($True) {
		try {

			$completed = & $cmd
			if ($completed -eq $false) {
				throw
			}

			if ($retrycount -gt 0) {
				Write-Host "Retry attempt $retrycount/$retries succeeded" -ForegroundColor "Green"
			}
			return $completed
		} 
		catch {
			if ($retrycount -eq $retries) {
				if ($retrycount -gt 0) {
					Write-Host "Retry attempt $retrycount/$retries failed" -ForegroundColor "Red"
				}
				throw
			} 
			Write-Error $PSItem
			$retrycount++
			Write-Host "An error occured. Starting retry attempt $retrycount/$retries after $secondsDelay second(s) delay" -ForegroundColor "Red"
			Start-Sleep $secondsDelay
		}
	}
}
 
<#
.SYNOPSIS
This is a helper function that runs a scriptblock and checks the PS variable $lastexitcode
to see if an error occcured. If an error is detected then an exception is thrown.
This function allows you to run command-line programs without having to
explicitly check the $lastexitcode variable.
.EXAMPLE
Invoke-Exec { svn info $repository_trunk } "Error executing SVN. Please verify SVN command-line client is installed"
.NOTES
Taken from psake https://github.com/psake/psake
#>
Function Invoke-Exec {
	[CmdletBinding()]
	param(
		[Parameter(Position = 0, Mandatory = 1)][scriptblock]$cmd,
		[Parameter(Position = 1, Mandatory = 0)][string]$errorMessage = ("Error executing command {0}" -f $cmd),
		[Parameter(Mandatory = 0)][switch] $IgnoreError = $false
	)

	$scriptExpanded = $ExecutionContext.InvokeCommand.ExpandString($cmd).Trim()
	InfoMessage "Executing command: $scriptExpanded"
	
	& $cmd | Out-Default
 
	if ($lastexitcode -ne 0) {
		$ErrorMessage = "Invoke-Exec failed with exit code '$lastexitcode' executing command " + $scriptExpanded
		if (-not($IgnoreError)) {
			throw $ErrorMessage
		}
		else {
			Write-Host "Ignoring Error: $ErrorMessage"
		}
	}

}
 
<#
.SYNOPSIS
Colourfully print an informational message so it appears in the log
#>
Function InfoMessage ([string] $message) { 
	Write-Host "$message`n" -ForegroundColor $infoMessageColour
}
 
<#
.SYNOPSIS
Colourfully print a warning message so it appears in the log
#>
Function WarnMessage ([string] $message) { 
	Write-Host "Warning: $message`n" -ForegroundColor "Yellow"
}		
	 
<#
.SYNOPSIS
finds the most recent file below this directory matching a pattern

.PARAMETER FilePathPattern
The path, with optional wildcards, to search for like "*.sln"

.PARAMETER Project
Defaults to current directorybut can be overridden with the directory (project name) that is passed in this parameter

.PARAMETER IgnoreError
If nothing is found and -IgnoreError=$false then throw an exception

.PARAMETER ExcludePattern
pass a pattern that will exclude items you do not want to match, e.g. "SomeOtherApp.sln"

.EXAMPLE
GetMostRecentFileMatchingPath "*.sln"
#>
Function GetMostRecentFileMatchingPath([string] $FilePathPattern, [string] $Project = "", [switch] $IgnoreError, [string] $ExcludePattern = $false) {
	if ($Project -eq "") {
		$Project = $SolutionFolder
	}

	$file = Get-ChildItem -Path $Project -Recurse -Filter $filePathPattern `
	| Where-Object { $_.FullName -NotMatch $ExcludePattern } `
	| Sort-Object LastWriteTime | Select-Object -last 1
	if ($file -eq "" -or $null -eq $file) {
		if (!$IgnoreError) {
			throw "Unable to find a file in $SolutionFolder (or below) matching $filePathPattern"
		}
		return $null;
	}
	return $file
}
 
<#
.SYNOPSIS
Find all the unique folders that contain a file matching a specification

.EXAMPLE
GetFoldersContainingFilesThatMatch "*.nuspec" "(packages)|(obj)"
#>
Function GetFoldersContainingFilesThatMatch([string] $FilePattern, [string] $ExcludePattern) {
	$items = Get-ChildItem -Filter $FilePattern -Recurse `
	| Select-Object -expandproperty FullName `
	| Get-Unique `
	| Where-Object { $_ -NotMatch $ExcludePattern } `
	| ForEach-Object { Split-Path $_ -Parent } 
 
	return $items
}

<#
.SYNOPSIS
Legacy function to invoke msbuild clean - use Invoke-Clean now
#>
Function Clean([string] $MSBuild = "${Env:ProgramFiles(x86)}\MSBuild\14.0\Bin\MSBuild.exe", [string] $Config = "Release") {
	WarnMessage "'Clean' function is deprecated and will be removed - use 'Invoke-Clean' or 'Invoke-CleanCore' instead"
	Invoke-Clean -MSBuild $MSBuild -Config $Config
}
 
<#
.SYNOPSIS
Start msbuild /t:clean for .net4 projects on the sln file in this directory. Use Invoke-CleanCore for dotnet clean cli/core projects.

.PARAMETER MSBuild
Pass a path to the version of MSbuild if you do not want to automatically detect and use the highest available

.PARAMETER Config
"Debug" or "Release"

.EXAMPLE
Invoke-Clean
#>
Function Invoke-Clean([string] $MSBuild = "(auto)", [string] $Config = "Release") {

	Invoke-BuildTask -Name "clean" -command {
		if ($MSBuild -eq "(auto)") {
			$MSBuild = Find-MsBuild
		}		 
		 
		InfoMessage "Clean step: Emptying $ReleaseDir"
		$_ = Remove-Item -path $ReleaseDir\* -recurse -force -ErrorAction silentlycontinue
		$_ = New-Item -path $ReleaseDir -type directory -force -ErrorAction silentlycontinue

		if (Test-Path $PathToSln) {
			$MsBuildCleanArgs = $PathToSln, "/t:clean", "/m", "/p:Configuration=$Config", "/noconsolelogger"
			InfoMessage "MsBuild Clean: `n  $MsBuild $MsBuildCleanArgs"
		
			Invoke-Exec -IgnoreError -cmd {
				& $MsBuild $MsBuildCleanArgs 
			}
		}
	}
}

<#
.SYNOPSIS
Start dotnet clean for core projects on the sln file in this directory. Use Invoke-Clean for .net 4 projects.

.PARAMETER Config
"Debug" or "Release"

.EXAMPLE
Invoke-CleanCore -Config "Release"
#>
Function Invoke-CleanCore(
	[string] $BuildCommand = "dotnet", 
	[string] $BuildArguments = "clean", 
	[string] $Config = "Release",
	[switch] $UseGitClean = $False) {
	$FinalMsBuildArgs = $BuildArguments, $PathToSln, "--configuration", $Config, "--nologo", "--verbosity", "minimal"

	Invoke-BuildTask -Name "clean" -command {
		InfoMessage "Empty release folder: Emptying $ReleaseDir"
		$filesToClean = Join-Path $ReleaseDir "*"
		$_ = Remove-Item -path $filesToClean -recurse -force -ErrorAction silentlycontinue
		$_ = New-Item -path $ReleaseDir -type directory -force -ErrorAction silentlycontinue
	
		if ($UseGitClean) {
			Write-Host "`nExecuting 'git clean -xdf'`n" -ForegroundColor "RED"

			Invoke-Exec -IgnoreError -cmd {
				git clean -xdf
			}
		} 
		else {
			if (Test-Path $PathToSln) {
				InfoMessage "Executing Clean: `n  $BuildCommand $FinalMsBuildArgs"
				Invoke-Exec -IgnoreError -cmd {
					& $BuildCommand $FinalMsBuildArgs
				}
			}
		}
	}
}
 
<#
.SYNOPSIS
Try and find the most recent version of msbuild. Optionally limit to a particular version. Will fall back to easlier versions if latest is not available

.PARAMETER MaxVersion
2019/2017/2015/2013

.EXAMPLE
Find-MsBuild
#>
Function Find-MsBuild([int] $MaxVersion = 2019) {
	$agentPath = "$Env:programfiles (x86)\Microsoft Visual Studio\2019\BuildTools\MSBuild\Current\Bin\msbuild.exe"
	$devPath = "$Env:programfiles (x86)\Microsoft Visual Studio\2019\Enterprise\MSBuild\Current\Bin\msbuild.exe"
	$proPath = "$Env:programfiles (x86)\Microsoft Visual Studio\2019\Professional\MSBuild\Current\Bin\msbuild.exe"
	$communityPath = "$Env:programfiles (x86)\Microsoft Visual Studio\2019\Community\MSBuild\Current\Bin\msbuild.exe"
	$agentPath2017 = "$Env:programfiles (x86)\Microsoft Visual Studio\2017\BuildTools\MSBuild\15.0\Bin\msbuild.exe"
	$devPath2017 = "$Env:programfiles (x86)\Microsoft Visual Studio\2017\Enterprise\MSBuild\15.0\Bin\msbuild.exe"
	$proPath2017 = "$Env:programfiles (x86)\Microsoft Visual Studio\2017\Professional\MSBuild\15.0\Bin\msbuild.exe"
	$communityPath2017 = "$Env:programfiles (x86)\Microsoft Visual Studio\2017\Community\MSBuild\15.0\Bin\msbuild.exe"
	$fallback2015Path = "${Env:ProgramFiles(x86)}\MSBuild\14.0\Bin\MSBuild.exe"
	$fallback2013Path = "${Env:ProgramFiles(x86)}\MSBuild\12.0\Bin\MSBuild.exe"
	$fallbackPath = "C:\Windows\Microsoft.NET\Framework\v4.0.30319\MSBuild.exe"
		 
	If ((2019 -le $MaxVersion) -And (Test-Path $agentPath)) { return $agentPath } 
	If ((2019 -le $MaxVersion) -And (Test-Path $devPath)) { return $devPath } 
	If ((2019 -le $MaxVersion) -And (Test-Path $proPath)) { return $proPath } 
	If ((2019 -le $MaxVersion) -And (Test-Path $communityPath)) { return $communityPath } 
	If ((2017 -le $MaxVersion) -And (Test-Path $agentPath2017)) { return $agentPath2017 } 
	If ((2017 -le $MaxVersion) -And (Test-Path $devPath2017)) { return $devPath2017 } 
	If ((2017 -le $MaxVersion) -And (Test-Path $proPath2017)) { return $proPath2017 } 
	If ((2017 -le $MaxVersion) -And (Test-Path $communityPath2017)) { return $communityPath2017 } 
	If ((2015 -le $MaxVersion) -And (Test-Path $fallback2015Path)) { return $fallback2015Path } 
	If ((2013 -le $MaxVersion) -And (Test-Path $fallback2013Path)) { return $fallback2013Path } 
	If (Test-Path $fallbackPath) { return $fallbackPath } 
	
	# to find msbuild in 2017+ consider using vswhere, see https://github.com/Microsoft/vswhere/wiki/Find-MSBuild

	throw "Yikes - Unable to find msbuild"
}
 
<#
.SYNOPSIS
Return a path to nuget.exe

.DESCRIPTION
 Return a path to nuget.exe. Tries the following locations in order:
 - find it in child folders such as Nuget.CommandLine/octopack packages folders
 - see if it is available on the path
 - try and download it from nuget.org to the packages folder 
 - throw error if it cannot be found

.EXAMPLE
$nugetPath = Find-Nuget
#>
Function Find-Nuget([string] $Executable = "nuget") {
	$localNugetPath = (GetMostRecentFileMatchingPath $Executable -IgnoreError)
	$NugetExePath = If ($localNugetPath -ne $null) { $localNugetPath.FullName } Else { "nuget" }
 
	# cant find it locally or path? then download
	if ((Get-Command $NugetExePath -ErrorAction SilentlyContinue) -eq $null) { 
		$sourceNugetExe = "https://dist.nuget.org/win-x86-commandline/latest/nuget.exe"
		$NugetExePath = "$SolutionFolder\packages\nuget.exe"
 
		WarnMessage "Unable to find nuget.exe in your PATH or in your project. Trying to get it from the web! Downloading $sourceNugetExe to $NugetExePath"
 
		mkdir "packages" -Force | Out-Null
		$result = Invoke-WebRequest $sourceNugetExe -OutFile $NugetExePath -PassThru -UseBasicParsing
 
		if ($result.StatusCode -ne 200) { 
			throw "Unable to download nuget from the web so Run 'Install-Package Nuget.CommandLine' to fix"
		}
		else {
			InfoMessage "Downloaded nuget to $NugetExePath"
		}
	}
 
	return $NugetExePath
}

<#
.SYNOPSIS
Restore packages using the nuget CLI or dotnet CLI

.DESCRIPTION
Use 'Restore' for .net4 and 'Restore -UseDotnet' for dotnet core

.PARAMETER UseDotnet
if passed use the 'dotnet restore' command, otherwise uses 'nuget restore'

.PARAMETER ToolsManifestFile
custom location for a tools file to eb restored

.EXAMPLE
Restore -UseDotnet

.NOTES
General notes
#>
Function Restore(
	[switch] $UseDotnet,
	[string] $ToolsManifestFile = ".config\dotnet-tools.json"
) {
	if ($UseDotnet) {
		$Executable = "dotnet"
	}
 else {
		$Executable = Find-Nuget
	}
	Invoke-BuildTask -Name "$Executable restore" -command {
		$CommandArgs = "restore", $SolutionFolder
		try {
			Push-Location $SolutionFolder			 
			Invoke-Exec { & $Executable $CommandArgs }

			$ToolsManifestFile = Join-Path $SolutionFolder $ToolsManifestFile
			# Restore dotnet tools if manifest file is discovered
			if (Test-Path $ToolsManifestFile) {
				InfoMessage "Restoring dotnet tools added in tool config file"
				Invoke-Exec { & dotnet tool restore --tool-manifest $ToolsManifestFile }
			}
  		}
		finally {
			Pop-Location
		}
	}
}
 
<#
.SYNOPSIS
Invoke msbuild on the sln. Deprecated. use Invoke-Build now.
#>
Function Build([string] $Config = "Release", [string] $MSBuild = "(auto)", [string] $MsBuildArgs = "") {
	WarnMessage "'Build' function is deprecated and will be removed - use 'Invoke-Build' or 'Invoke-BuildCore' instead"
 
	Invoke-Build -Config $config -MsBuild $MsBuild -MsBuildArgs $MsBuildArgs
}	

<#
.SYNOPSIS
Invoke MSBuild on the adjacent sln file. Default to Release. Use for .net4 project.  Use Invoke-BuildCore for dotnet core projects.
OctoPack is automatically detected and enabled if available. Outputs to the ../dist/{project} folder

.PARAMETER MSBuild
Custom path to msbuild exe

.PARAMETER MsBuildArgs
other args to pass to msbuild

.PARAMETER Config
"Debug" or "Release"

.EXAMPLE
Invoke-Build -Config "Release"
#>
Function Invoke-Build([string] $MSBuild = "(auto)", [string] $MsBuildArgs = "", [string] $Config = "Release") {
	if ($MSBuild -eq "(auto)") {
		$MSBuild = Find-MsBuild
	}

	Invoke-BuildTask -Name "msbuild" -command {
 
		InfoMessage "Build: Compiling $PathToSln in $Config configuration"
		InfoMessage "Log file: $LogFile"
 
		$UseOctopack = $false
		If ($null -ne (GetMostRecentFileMatchingPath "OctoPack.Tasks.dll" -IgnoreError)) {
			InfoMessage "Detected you have Octopack installed - Adding RunOctoPack=true to MsBuild parameters for automatic packing"
			$UseOctopack = $true
		}
 
		$OctopackMsbuildParams = If ($UseOctopack) { "/p:RunOctoPack=true;OctoPackPublishPackageToFileShare=$ReleaseDir;OctoPackPublishPackagesToTeamCity=false" } else { "" }
	 
		$FinalMsBuildArgs = $MsBuildArgs, $PathToSln, "/p:Configuration=$config", "/p:OutputPath=$ReleaseDir\$ProjName", $OctopackMsbuildParams, "/t:build", "/noautorsp", "/ds", "/m", "/l:FileLogger,Microsoft.Build.Engine;logfile=$LogFile"
 
		InfoMessage "Executing MsBuild: `n  $MSBuild $FinalMsBuildArgs"
 
		Invoke-Exec { 
			& $MsBuild $FinalMsBuildArgs 
		}
	}
}
 
<#
.SYNOPSIS
Invoke dotnet build on the adjacent sln file. Default to Release. Use for dotnet core projects.  Use Invoke-Build for .net 4 projects.
Version numbers applied automatically. Assumes alreay restored. Outputs to the ../dist/{project} folder

.PARAMETER Config
Debug or Release

.PARAMETER VersionSuffix
Append a custom semver suffix so "aplha01" => "1.0.0.0-alpha01"

.EXAMPLE
Invoke-BuildCore -Config "Release"
Invoke-BuildCore -Config $Config -VersionSuffix $NugetVersionSuffix
#>
Function Invoke-BuildCore([string] $BuildCommand = "dotnet", [string] $BuildArguments = "build", [string] $Config = "Release", [string] $VersionSuffix = "") {
Write-Host "Info: zx1x: " + $BuildNumber
Write-Host "Info: zx2x: " + $BuildArguments

	Invoke-BuildTask -Name "$BuildCommand $BuildArguments" -command {
Write-Host "Info: zx3x: " + $BuildNumber
Write-Host "Info: zx4x: " + $VersionSuffix
		$buildVersion = $BuildNumber + $VersionSuffix
		If ($VersionSuffix -ne "") {
			InfoMessage "Building solution with pre-release version $buildVersion"
		}
		InfoMessage "Using $BuildCommand $BuildArguments to compile $PathToSln in $Config configuration with version number $buildVersion"
		InfoMessage "Log file: $LogFile"
 
		$assemblyVersion = $buildVersion -match '\d+\.\d+\.\d+\.?(\d+)?' | % { $Matches[0] }
		$FinalMsBuildArgs = $BuildArguments, $PathToSln, "--no-restore", "--configuration", $Config, "/p:AssemblyVersion=$assemblyVersion", "/p:Version=$buildVersion", "/flp:logfile=$LogFile"
 
		InfoMessage "Executing Build: `n  $BuildCommand $FinalMsBuildArgs"
 
		Invoke-Exec { & $BuildCommand $FinalMsBuildArgs }
	}
}

<#
.SYNOPSIS
Invoke dotnet pack command on a project and output to ../dist/{project}

.PARAMETER Project
Name of project folder to be packed

.PARAMETER Config
"Debug" or "Release"

.PARAMETER OutputPath
Custom output path. defaults to ../dist

.PARAMETER VersionSuffix
Append a custom semver suffix so "aplha01" => "1.0.0.0-alpha01"

.EXAMPLE
Invoke-PackCore "MyNewApp" -Config "Release"
#>
Function Invoke-PackCore(
	[string] $Project, 
	[string] $Config = "Release", 
	[string] $OutputPath = "$ReleaseDir",
	[string] $VersionSuffix = "") {
	Invoke-PublishCore -Project $Project -BuildCommand dotnet -BuildArguments pack -Config $Config -OutputPath $OutputPath -VersionSuffix $VersionSuffix
}
 
<#
.SYNOPSIS
Invoke dotnet publish command on a project and output to ../dist/{project}
Assumes project is already built. Places a publish log file in artefacts directory. Fails teamcity if publish fails.

.PARAMETER Project
Name of project folder to be packed

.PARAMETER Config
"Debug" or "Release"

.PARAMETER OutputPath
Custom output path. defaults to ../dist

.PARAMETER VersionSuffix
Append a custom semver suffix so "aplha01" => "1.0.0.0-alpha01"

.EXAMPLE
Invoke-PublishCore "MyNewApp" -Config "Release"
Invoke-PublishCore "MyNewApp" -Config "Release" -VersionSuffix $NugetVersionSuffix
#>
Function Invoke-PublishCore(
	[string] $Project, 
	[string] $BuildCommand = "dotnet", 
	[string] $BuildArguments = "publish", 
	[string] $Config = "Release",
	[string] $OutputPath = "",
	[string] $VersionSuffix = "") {
	Invoke-BuildTask -Name "dotnet publish" -command {
		$buildVersion = $BuildNumber + $VersionSuffix
		$OutputPath = if($OutputPath -eq "") { Join-Path $ReleaseDir $Project } else { $OutputPath}
		If ($VersionSuffix -ne "") {
			WarnMessage "Publishing ($BuildCommand $BuildArguments) project $Project with pre-release version $buildVersion in $Config configuration"
		} 
		else {
			InfoMessage "Publishing ($BuildCommand $BuildArguments) project $Project with version number $buildVersion in $Config configuration"
		}
		$ProjectPublishLog = Join-Path $ReleaseDir "$Project-Publish.log"
		InfoMessage "Publish Log file: $ProjectPublishLog"
		$projectToPublish = Join-Path $SolutionFolder $Project

		$FinalDotnetArgs = $BuildArguments, $projectToPublish, "--output", $OutputPath, "--no-restore", "--configuration", $Config, "/p:Version=$buildVersion", "/flp:logfile=$ProjectPublishLog"

		Invoke-Exec {
		 & $BuildCommand $FinalDotnetArgs
		}

		if (Test-Path $ProjectPublishLog) {
			Get-Content $ProjectPublishLog -OutVariable PublishResult | out-null

			if ($PublishResult.IndexOf("Build succeeded.") -lt 0) {
				throw "Publish $Project FAILED! See msbuild log: $ProjectPublishLog"
			} 
			else {
				InfoMessage "`nMsBuild publish log file reports success ($ProjectPublishLog)"
			}
		}
	}
}
 
<#
.SYNOPSIS
Execute any nunit tests identified by tests naming convention "*Tests.dll". Deprecated. Use Invoke-TestNUnit instead
#> 
Function Test-NUnit([string] $Assembly = "*Tests.dll", [string] $NUnitConsole = "nunit3-console.exe", [string] $Config = "Release") {
	WarnMessage "'Test-NUnit' function is deprecated and will be removed - use 'Invoke-TestNunit' for nunit3-console or 'Invoke-TestCore' for dotnet test instead"
	Invoke-TestNUnit -Assembly $Assembly -NUnitConsole $NUnitConsole -Config $Config
}		
 
<#
.SYNOPSIS
Run NUnit tests using the nunit console test runner. Finds all tests DLLs matching a pattern and executes the nunit tests
Will report results to Teamcity (if available) and outputs the test result file to the Release directory
TODO: each call should produce different test file outpus ($NunitTestOutput) but at the moment they overwrite each other

.PARAMETER Assembly
pattern to tests dlls, defaults to "*Tests.dll"

.PARAMETER Config
"Debug" or "Release"
#>
Function Invoke-TestNUnit([string] $Assembly = "*Tests.dll", [string] $NUnitConsole = "nunit3-console.exe", [string] $Config = "Release") {
	Invoke-BuildTask -Name "dotnet test $Assembly" -command {

		$NUnitConsolePath = (GetMostRecentFileMatchingPath $NUnitConsole -IgnoreError).FullName
 
		if ($NUnitConsolePath) {
			InfoMessage "NUnit tests: Finding tests matching $Assembly in bin folders."
		 
			# Find tests in child folders (except obj)
			$TestDlls = Get-ChildItem -Path $SolutionFolder -Recurse  -Include $Assembly | Select-Object -expandproperty FullName | Where-Object { $_ -NotLike "*\obj\*" } | % { "`"$_`"" }
 
			if (@($TestDlls).Count -eq 0) {
				# Add the tests in the output folders (except obj)
				$TestDlls += Get-ChildItem -Path $ReleaseDir -Recurse -Include $Assembly | Select-Object -expandproperty FullName | Where-Object { $_ -NotLike "*\obj\*" } | % { "`"$_`"" }
			}
 
			$teamcityOption = If ($IsTeamcity) { "--teamcity" } Else { "" }
 
			$NUnitArgs = ("/config:$Config", "/process:Multiple", "--result=TestResult.xml", $teamcityOption)
			$NUnitArgs += $TestDlls
 
			InfoMessage "Found $(@($TestDlls).Count) test DLL(s): $TestDlls. Test Output will save to $nunitTestOutput)"
			InfoMessage "Executing Nunit: `n  $NUnitConsolePath $NUnitArgs     " 
 
			if (@($TestDlls).Count -eq 0) {
				WarnMessage "No tests found!"
			}
			else {
				& $NUnitConsolePath $nunitArgs 2>&1 # redirect stderr to stdout, otherwise a build with muted tests is reported as failed because of the stdout text
					 
				InfoMessage "Placing test result file at $NunitTestOutput"
				Move-Item -Path "TestResult.xml" -Destination $NunitTestOutput
			}
		}
		else {
			InfoMessage "Skipping Test-NUnit() - no nunit console available"
		}
	}
}

<#
.SYNOPSIS
Parse a csproj file to extract the generated assembly name from the XML
#>
Function Get-AssemblyNameFromCsProj {
	param (
		[string] $ProjectFolder
	)
		 
	Try {
		$csProjFile = (Get-ChildItem $ProjectFolder -Filter *.csproj)[0]
		$AssemblyName = $csProjFile.BaseName
	}
 Catch {
		Throw "Unable to locate csproj file in '$ProjectFolder'"
	}
 
	$CsProjXml = ([xml](Get-Content $csProjFile.FullName)).Project
	if (Get-Member -inputobject $CsProjXml.PropertyGroup -Name "AssemblyName" -MemberType Properties) {
		$AssemblyName = $CsProjXml.PropertyGroup.AssemblyName
	}
	$AssemblyName
}

<#
.SYNOPSIS
Get the path param with quotes and trailing slashes added if needed ready to pass as args to a command

.EXAMPLE
Get-EscapedPath "\some\path"             # return the string: \some\path
Get-EscapedPath "\some\path with space"  # return the string "\some\path with space"
Get-EscapedPath "\some\path with space\" # return the string "\some\path with space\\"
#>
function Get-EscapedPath {
	param (
		[string] $path
	)
		
	# if path has a space we need to wrap in quotes
	if ($path -match " ") {
		# if path ends in slash then we need to avoid \" at the end by double slashing to become \\"
		if ($path -match '\\$') {
			$path += '\'
		}
		return "`"" + $path + "`""
	}
 else {
		return $path
	}
}

<#
.SYNOPSIS
Discovers unit and integration tests and invokes a test CLI tool to execute them. Defaults to `dotnet test`

.DESCRIPTION
Uses a folder naming convention to discover and execute your unit and integration test projects using a command line test executor such as `dotnet test`. Works well with nunit/xnuit and teamcity. You can use `Invoke-TestCore` in the solution root folder to discover the all the unit test projects automatically, and execute the all the tests using the dotnet CLI `dotnet test`. 

By default `Invoke-TestCore` will:
* Locate all projects matching the pattern ProjectNameFilter and execute them sequentially
* Assume your project has already been restored/compiled (using `dotnet build`)
* Use sensible defaults, like using the dotnet test CLI in Release config but allow you to override them
* Output a code coverage report to the $ReleaseDir using coverlet
* Tell teamcity about your test coverage using Teamcity service messages
* Exclude projects named *Tests* from code coverage stats, and makes this configurable.

.PARAMETER TestCommand
The CLI test runner tool to execute. Defaults to `dotnet`

.PARAMETER TestArguments
The CLI test runner tool arguments to be passed to TestCommand. Defaults to `test`

.PARAMETER Config
The build configuration, usually Debug or Release. Defaults to Release.

.PARAMETER ProjectNameFilter
A Get-ChildItem filter to be used to locate the folders that contain test projects. Defaults to *UnitTest* and would match folders named MyApp.MyProj.UnitTests or MyApp.UnitTest

.PARAMETER SkipCodeCoverage
A bool switch to disable coverlet code coverage stats being generated. Defaults to False.

.PARAMETER CoverletExcludes
Specify a string to define assembly or project exclusions that are passed to coverlet (https://github.com/tonerdo/coverlet). 
Defaults to "[*.*Tests?]*" which excludes assembly names containing 'Test' or 'Tests' from code coverage reports since we usually don't want the coverage of the classes in a unit test project inflating production code coverage stats. Also accepts an array such as:
"[*.UnitTests]*","[*.IntegrationTests]*"

.EXAMPLE
Given a project MyApp.dll with a Unit Test project MyApp.UnitTests.dll you can execute the following Powershell in the solution root to locate the unit tests and execute them and output code coverage stats:

PS$> Invoke-TestCore

.EXAMPLE
Given a project MyApp.dll with a Test project MyApp.IntegrationTests.dll built in debug configuration you can execute the following Powershell in the solution root to locate the unit tests and execute them and output code coverage stats:

PS$> Invoke-TestCore -Config Debug -ProjectNameFilter "*.IntegrationTests"

.EXAMPLE
Given a project MyApp.dll with a Test project MyApp.IntegrationTests.dll built in debug configuration you can execute the following Powershell in the solution root to locate the unit tests and execute them and output code coverage stats:

PS$> Invoke-TestCore -Config Debug -ProjectNameFilter "*.IntegrationTests"

.NOTES
Typically for each test project you need to have the following:

$PS> dotnet add package Microsoft.NET.Test.Sdk
$PS> dotnet add package NUnit   
$PS> dotnet add package NUnit3TestAdapter
$PS> dotnet add package TeamCity.VSTest.TestAdapter
#>

Function Invoke-TestCore(
	[string] $TestCommand = "dotnet", 
	[string] $TestArguments = "test", 
	[string] $Config = "Release", 
	[string] $ProjectNameFilter = "*UnitTest*",
	[switch] $SkipCodeCoverage = $false,		
	[string[]] $CoverletExcludes = "[*.*Tests?]*"
) {
	#TODO consider dotnet vstest (Get-ChildItem -recurse -File *.Tests.*dll | ? { $_.FullName -notmatch "\\obj\\?" }) for agregated test run

	if (-Not($SkipCodeCoverage) -and -not ($testToolsInstalled)) {
		# Get code coverage dependencies
		Install-Tool -InstalledExePath reportgenerator.exe -ToolName dotnet-reportgenerator-globaltool
		Install-Tool -InstalledExePath coverlet.exe -ToolName coverlet.console
		$script:testToolsInstalled = $true
	}

	Invoke-BuildTask -Name "dotnet test $ProjectNameFilter" -command {
		if ((Test-Path $TestCommand) -Or $null -ne (Get-Command $TestCommand -ErrorAction SilentlyContinue)) {
				 
			InfoMessage "Tests: Finding tests matching $ProjectNameFilter below $SolutionFolder"
 
			$TestFolders = Get-ChildItem -filter $ProjectNameFilter -Directory -Recurse -Path $SolutionFolder
			$ReporterFolder = (Resolve-Path $ReleaseDir)
				 
			InfoMessage "Found $(@($TestFolders).Count) test folder(s): $TestFolders"
 
			$TestFolders | ForEach-Object {
				$TestFolder = $_.FullName
				$ReportParam = "-r:" + (Get-EscapedPath $ReporterFolder)
				$FinalTestArgs = $TestArguments, (Get-EscapedPath $TestFolder.TrimEnd('\')), "-c:$Config", $reportParam, "--no-build", "--no-restore"
 
				if ($SkipCodeCoverage) {
					InfoMessage "Executing Tests (without code coverage):"
					Invoke-Exec { 
						& $TestCommand $FinalTestArgs 
					}
				} 
				else {
					$AssemblyName = Get-AssemblyNameFromCsProj -ProjectFolder $_.FullName
					$AssemblyPath = Get-ChildItem -Path $TestFolder\bin\$Config -Recurse -Include "$AssemblyName.dll" `
					| Where-Object { $_ -NotMatch "\\ref\\" }`
					| Select-Object -first 1
					InfoMessage "Instrumenting $AssemblyPath for code coverage before testing"
						 
					# build a single string of dotnet test args to be passed to coverlet --targetargs. Quotes must be escaped with \
					$CoverletTargetArgs = ($FinalTestArgs -Join " ").Replace('"', '\"')

					$command = "coverlet"
					$coverageJsonFile = Get-EscapedPath (Join-Path $CoverageDataFolder "coverage.json")
					# build the array of args to pass to coverlet. VERY fussy about quotes and slashes escaping because --targetargs is taking a string full of command arguments
					$commandArgs = [System.Collections.ArrayList] ( `
						(Get-EscapedPath $AssemblyPath), `
							"--target", $TestCommand, `
							"--targetargs", (Get-EscapedPath $CoverletTargetArgs), `
							"--merge-with", $coverageJsonFile, `
							"--format", "json", `
							"--format", "opencover", `
							"--output", (Get-EscapedPath "$CoverageDataFolder\"))

					foreach ($CoverletExclude in $CoverletExcludes) {
						$commandArgs.Add("--exclude") | Out-Null
						$commandArgs.Add((Get-EscapedPath $CoverletExclude)) | Out-Null
					}

					if ($IsTeamcity) { 
						$commandArgs.Add("--format") | Out-Null
						$commandArgs.Add("teamcity") | Out-Null
					}

					InfoMessage "Executing Test (with code coverage): `n$command $commandArgs"
					# because the args for coverlet are nightmare of quotes and spaces the call operator (&) throws errors so we use start process here
					$p = Start-Process -FilePath $command -ArgumentList $commandArgs  -Wait -NoNewWindow -PassThru
					if ($p.ExitCode -ne 0) {
						throw ("Coverlet failed with exit code '$($p.ExitCode)'")
					}
				}
			}
 
			if (-Not($SkipCodeCoverage) -and @($TestFolders).Count -gt 0) {
				Write-Host "Generating coverage html report to $CoverageDataFolder"
				$reportArgs = (Get-EscapedPath "-reports:$CoverageDataFolder\coverage.opencover.xml"), `
				(Get-EscapedPath "-targetdir:$CoverageDataFolder\"), `
					"-reporttypes", `
					"HTML"
				# TODO OPTIONALLY Filter this?? -assemblyfilters:"-nunit*;-*.test.dll;-*.tests.dll"
							
				Invoke-Exec {
					reportgenerator $reportArgs
				}
				
				if ($IsTeamcity) { 
					Write-Host "Publishing coverage.zip to $ReleaseDir"
					Compress-Archive -Path $CoverageDataFolder\* -DestinationPath $ReleaseDir\Coverage.zip -Force
					Get-ChildItem -Recurse -File -path $CoverageDataFolder | Where-Object { ($_.Extension -ne ".json" -AND $_.Extension -ne ".xml") } | Remove-Item -Force
				}
			}
				 
			if (@($TestFolders).Count -eq 0) {
				WarnMessage "No tests found!"
			}
		}
		else {
			InfoMessage "Skipping Test-Core() - could not find $TestCommand"
		}
	}
}
 
<#
.SYNOPSIS
Find all folders with .nuspec files and pack them using nuget. Deprecated. prefer dotnet pack now using Invoke-PackCore or use New-ZipPackage for Zips
#>
Function Pack([string] $Executable = "nuget.exe", [string] $Config = "Release") {
	Invoke-BuildTask -Name "nuget pack" -command {
		# use nuget.exe from package Nuget.CommandLine/octopack if possible, else if it is on path, use that
		$NugetExePath = Find-Nuget $Executable
 
		$FoldersWithNuspecs = GetFoldersContainingFilesThatMatch "*.nuspec" "(packages)|(obj)"
		ForEach ($projectFolder In $FoldersWithNuspecs) {
			# assuming here that we want to ensure the folder has a csproj
			If (Test-Path (Join-Path $projectFolder "*.csproj")) {
				$projectToPack = Join-Path $projectFolder "*.csproj" -Resolve
				$NugetArgs = "Pack", "$projectToPack", "-Properties", "Configuration=$Config", "-OutputDirectory", "$ReleaseDir"
					 
				InfoMessage "Executing pack on $projectToPack `n   $NugetExePath $NugetArgs" 
				& $NugetExePath $NugetArgs
			}
		}
 
		if (@($FoldersWithNuspecs).Count -eq 0) {
			InfoMessage "Skipping Pack step - no *.nuspec files found to Pack"
		}
	} 
}
 
<#
.SYNOPSIS
Install a new CLI tool GLOBALLY using dotnet tool install. Tools are updated automatically to latest version every 30days

.PARAMETER InstalledExePath
Once installed, what is the relative path from ToolsFolder to the executable, e.g. "dotnet-octo.exe" 

.PARAMETER ToolName
nuegt package name for the dotnet tool, e.g. "dotnet-octo"

.PARAMETER ToolsFolder
Full path of folder to install tool into. Defaults to "c:\tools\dotnet"

.PARAMETER DaysBeforeUpdate
How many days since the last dotnet tool install/update before forcing a tool update from source	 

.PARAMETER Version
Which version of the tool to install

.EXAMPLE
Install-Tool -InstalledExePath coverlet.exe -ToolName coverlet.console

#>
Function Install-Tool {
	Param
	(
		[Parameter(Mandatory = $true)]
		[string] $InstalledExePath,
		[Parameter(Mandatory = $true)]
		[string] $ToolName,
		[string] $ToolsFolder = (Join-Path "\tools" "dotnet"),
		[int]    $DaysBeforeUpdate = 30,
		[string] $Version = $null
	)
	Invoke-BuildTask -Name "dotnet tool install $ToolName" -command {
		$toolPath = Join-Path $ToolsFolder $installedExePath
		$needUpdate = $false
		$lastUpdate = (Get-Item "$toolPath*" -ErrorAction SilentlyContinue)
		$command = "install"

		if ($lastUpdate) { 
			$command = "update" 
			$needUpdate = (((Get-Date) - $lastUpdate.LastWriteTime) -gt (New-timespan -days $DaysBeforeUpdate))
		} 

		$commandArgs = [System.Collections.ArrayList] ( `
				"tool", `
				$command, `
				$ToolName, `
				"--tool-path", $ToolsFolder)

		if ($null -ne $Version -and $Version.Length -gt 0) {
			$commandArgs.Add("--version") | Out-Null
			$commandArgs.Add($Version) | Out-Null
		}

		if (($command -eq "install") -or $needUpdate) {
			Invoke-Exec { & dotnet $commandArgs }
		}
		else {
			$lastWrite = $lastUpdate.LastWriteTime.ToString("dd/MM/yyyy")
			InfoMessage "Skipping tool update for $toolPath as last update ($lastWrite) was less than $DaysBeforeUpdate days ago"
		}
 
		if (-not (Test-Path $toolPath*)) {
			throw "Unable to find tool installed at $toolPath - Install probably failed. Check params InstalledExePath and ToolName"
		}
		 
		$ToolsFolderPattern = "$ToolsFolder;".Replace("\", "\\")
		If (-Not ($env:PATH -match $ToolsFolderPattern )) {
			# Add the tools path to the PATH env variable
			InfoMessage "Adding $ToolsFolder to your PATH"
			$env:PATH = "$ToolsFolder;" + $env:PATH
		}
	}
}
 
<#
.SYNOPSIS
Installs dotnet octo CLI tool if not available and adds it to your path for the rest of your session. Will do an update if octo is out of date.

.DESCRIPTION
Default install location is c:\tools\dotnet which is added to your PATH. once this is run you can run `dotnet octo`

.EXAMPLE
Install-Octo
#>
Function Install-Octo() {
	WarnMessage "Install-Octo has been deprecated - use as a local tool instead by running 'dotnet tool install Octopus.DotNet.Cli' and remove 'Install-Octo' from your build script"
	Install-Tool -InstalledExePath dotnet-octo -ToolName Octopus.DotNet.Cli
}
	 
<#
.SYNOPSIS
Ensures that a dotnet tool is installed, and installes the latest version if required

.PARAMETER ToolName
Tool to be installed
#>
Function Initialize-DotnetTool ([string] $ToolName) {

	if ($null -eq $ToolName -Or $ToolName -eq "") { throw "ToolName cannot be empty" }

	if ($script:installedTools -contains $ToolName) {
		return
	}
	if (-not(dotnet tool list | Out-String | Select-String $ToolName)) { 
		WarnMessage "Unable to find $ToolName installed as a dotnet core local tool. Please add it to the tools manifest. Fetching the latest version" 
	
		Invoke-Exec { 
			dotnet tool install $ToolName --verbosity minimal
		}
	}
	$script:installedTools += $ToolName
	InfoMessage "$ToolName initialised"
}
$script:installedTools = @()
<#
.SYNOPSIS
Creates a zip file of a directory containing terraform files ready for deployment

.DESCRIPTION
Creates a zip file of a directorys terraform files and outputs it to a parent directory called dist ready for deployment. This package can then be sent to octopus deploy

.PARAMETER id
The ID of the package to create

.PARAMETER version
The Semver version number of the package to create

.PARAMETER FolderToZip
The folder to be zipped

.PARAMETER outputFolder
Where to place the new zip file

.PARAMETER Include
pattern for files to be included in zip by default. Defaults to *.*. For more an one comma seperate.

.PARAMETER OctoPackArgs
Additional args to be passed to dotnet octo. For more an one comma seperate.

.PARAMETER RemoveFolder
Should the source FolderToZip be removed after sucessful zipping?

.EXAMPLE
New-ZipPackage -id "hello-world" -version "1.0.0.0" -FolderToZip "c:\proj\hello\src" -outputFolder "c:\proj\hello\dist" -Include "*.tf","*.tfvars"
#>
Function New-ZipPackage(
	[String] $Id, 
	[String] $Version, 
	[String] $FolderToZip, 
	[String] $OutputFolder, 
	[String[]] $Include = "**\*.*", 
	[String[]] $OctoPackArgs = "", 
	[switch] $RemoveFolder = $false) {
	return Invoke-BuildTask -Name "dotnet octo pack $Id" -command {

		if((Test-Path $FolderToZip -ErrorAction SilentlyContinue) -and (Test-Path $OutputFolder -ErrorAction SilentlyContinue)){
			InfoMessage "Zipping files in '$FolderToZip' to $OutputFolder"
		} else {
			WarnMessage "Unable to zip files in '$FolderToZip' to '$OutputFolder'"
			return $false
		}

		$zipPath = ""
		try { 
			Push-Location $SolutionFolder
			Initialize-DotnetTool "octopus.dotnet.cli"
 
			$format = "zip"
			$FolderToZip = Resolve-Path $FolderToZip
			$OutputFolder = Resolve-Path $OutputFolder
		 
			# Build a cli command to pack the files into a zip with version number from Teamcity
			$command = "dotnet"
			$commandArgs = [System.Collections.ArrayList] ("octo", "pack", "--id=$Id", "--version=$Version", "--basePath=$FolderToZip", "--outFolder=$OutputFolder", "--format=$format")
 
			foreach ($includePattern in $Include) {
				$commandArgs.Add("--include=$IncludePattern") | Out-Null
			}
			foreach ($octoPackArg in $OctoPackArgs) {
				$commandArgs.Add($octoPackArg) | Out-Null
			}
 
			Invoke-exec {
				& $command $commandArgs
			}
			
			if ($RemoveFolder) {
				Remove-Item $FolderToZip -Recurse -Force -ErrorAction SilentlyContinue | Out-Null
			}
			
			# Return path to created package
			$zipPath = (Get-ChildItem "$outputFolder\$id.$version.$format" | Select-Object -First 1).FullName
			InfoMessage "Zipped files to $zipPath"
		}
		finally {
			Pop-Location
		}
		return $ZipPath
	}
}

<#
 .SYNOPSIS
 Convert timespan to human readable format
 #>
Function Get-PrettyDuration([timespan] $duration) {

	$hrs = [Math]::Floor($duration.TotalHours);
	$ms = [Math]::Floor($duration.TotalMinutes);
	$s = [Math]::Ceiling($duration.Seconds);
	$ss = $duration.Seconds.ToString("#0.#");

	if ($hrs -gt 0) { return "$($hrs)h $($ms)m $($s)s" }
	if ($ms -gt 0) { return "$($ms)m $($s)s" }
	return "$($ss)s" 
}

function Get-MessageEscapedForTeamcity ([string] $text) {
	return $text.Replace("|", "||").Replace("'", "|'").Replace("\n", "|n").Replace("\r", "|r").Replace("[", "|[").Replace("]", "|}").Replace('`n', '')
}
 
<#
.SYNOPSIS
Scan build outputs and raise appropriate failure errors or report sucess. Sets Teamcity build pass/fail if on build server.
.PARAMETER failMessage
If passed, force build to fail with this message. If empty assumed to be a passing build.
#> 
Function ReportResults([string] $failMessage = "") {
	
	$failMessage = ""
	$stopwatch.Stop()
	$duration = Get-PrettyDuration $stopwatch.Elapsed

	if (Test-Path $LogFile) {
		Get-Content $LogFile -OutVariable BuildSolutionResult | out-null
 
		if ($BuildSolutionResult.IndexOf("Build succeeded.") -lt 0) {
			$failMessage = "MsBuild FAILED! See msbuild log: $LogFile "
		} 
		else {
			InfoMessage "`nMsBuild log file reports success ($LogFile)"
		}
	}
 
	# check the NUnit tests file for failures
	if (Test-Path $nunitTestOutput) {
		Get-Content $nunitTestOutput -OutVariable testResult | out-null
 
		if ($testResult -match 'result="Failed"' -and $failMessage -eq "") {
			$failMessage = "Tests FAILED! See $nunitTestOutput "
		}
		else {
			InfoMessage "`nNUnit log file reports success ($nunitTestOutput)"
		}
	}
 
	if ($buildTasks) {
		$summary = Get-BuildTasksSummary

		# Show build summary table. Teamcity is fussy about with so use Out-String
		$buildTasks | Format-Table `
		@{L = 'Build Task'; E = { $_.BuildTask } },
		@{L = 'Succeeded'; E = { if ($_.Skipped) { "-" } else { "$($_.Succeeded)" } }; Alignment = 'center' },
		@{L = 'Skipped'; E = { if ($_.Skipped) { "Yes" } else { "-" } }; Alignment = 'center' },
		@{L = 'Duration'; E = { Get-PrettyDuration (New-TimeSpan -Seconds $_.Duration) }; Alignment = 'right'; },
		@{L = '% of Build'; E = { $_.Percentage }; Alignment = 'right'; } | Out-String -Stream -Width  100

		If (-not ($summary.Succeeded)) {
			$failMessage += "Task '$($summary.FirstFailure)' FAILED. $($summary.TasksFailed) build task(s) failed in $duration. ($($summary.TasksSkipped) skipped)."
				
			if ($IsTeamcity) {
				$teamcityEscapedMessage = Get-MessageEscapedForTeamcity $failMessage
				Write-Host "##teamcity[buildProblem description='$teamcityEscapedMessage']"
			}
				
			Write-Host  $failMessage -ForegroundColor "Red"
		} 
		else {
			$message = "$($summary.TasksSucceded) Tasks completed successfully in $duration. ($($summary.TasksSkipped) skipped)."
			Write-Host "Success! $message" -ForegroundColor "Green"

			if ($IsTeamcity) {
				$teamcityEscapedMessage = $failMessage.Replace("'", "|'")
				Write-Host "##teamcity[buildStatus text='{build.status.text}. $message']"
			}
		}
	}
}
 
<#
.SYNOPSIS
When running under teamcity, inform it of a folder to be considered as build artifacts

.PARAMETER folder
folder path to be published

.EXAMPLE
Publish-TCArtifacts "c:\proj\hello\dist"
#>
Function Publish-TCArtifacts([String] $folder) {
	$Skip = -not $IsTeamcity -or -not (Test-Path $folder -ErrorAction SilentlyContinue)
	$folderName = if ($skip) { "" } else { Split-Path $folder -Leaf }

	Invoke-BuildTask -Name "TeamCity Artifact publish $folderName" -Skip:$Skip -command {
		If ($IsTeamcity -and (Test-Path $folder)) { 
			Write-Host "##teamcity[publishArtifacts '$folder']" 
		}
		else {
			Write-Host "Teamcity Artifact publish skipped" -ForegroundColor Yellow
		}
	}
}
 
<#
.SYNOPSIS
Push a package to an Octopus Deploy Server

.DESCRIPTION
If you have env variables set fopr env:\Octopus_Url and env:\Octopus_ApiKey then push the specified package to that server using the key

.PARAMETER packagePath
path of the .zip or .nupkg to be published

.EXAMPLE
Publish-ToOctopusGallery "c:\proj\hello\dist\hello.1.0.0.0.zip"
#>
Function Publish-ToOctopusGallery ([String] $packagePath) {
	try {
		$packageId = $packagePath -match '\\?([\w\.\-]+?)\.(\d+\.)[3-4]?' | ForEach-Object { $Matches[1] }
	} catch {
		$packageId = ""
	}
	[bool] $hasKeys = (Test-Path env:\Octopus_Url) -and (Test-Path env:\Octopus_ApiKey) -and "$($env:Octopus_ApiKey)".Length -gt 3
	[bool] $hasPath = "$packagePath".Length -gt 0
	$skip = -not $IsTeamcity -or -not $hasPath -or -not $hasKeys

	if ($packagePath -and $skip) {
		Write-Host "Cannot publish package locally, or without Octopus_Url and Octopus_ApiKey env variables" -ForegroundColor Yellow
	}

	Invoke-BuildTask -Name "dotnet octo push $packageId" -Skip:$skip -command {
		Write-Host "Publishing $packagePath to $env:Octopus_Url"
		
		Invoke-Exec {
			& dotnet octo push --package $packagePath --server $env:Octopus_Url --apiKey $env:Octopus_ApiKey
		}
	}
	
}

<#
.SYNOPSIS
If on the master/main branch then push a nuget .nupkg package to a nuget server using NUGET_URL and NUGET_APIKEY environment variables

.DESCRIPTION
Long description

.PARAMETER packagePath
Parameter description

.EXAMPLE
An example

.NOTES
General notes
#>
Function Publish-ToNugetGallery ([String] $packagePath) {

	Invoke-BuildTask -Name "nuget push $packagePath" -command {

		Write-Host "Ready to publish $packagePath (On branch $BranchName)"
	
		If (($IsTeamcity) `
				-and (Test-Path env:\Nuget_Url) `
				-and (Test-Path env:\Nuget_ApiKey) `
				-and $IsMasterBuild) {
			
			$nugetExePath = Find-Nuget "nuget.exe"
			$nugetArgs = "push", "$packagePath", "-Source", $env:Nuget_Url, "-ApiKey", $env:Nuget_ApiKey
			
			Invoke-Exec { & $nugetExePath $nugetArgs }
	
		}
	}
}
	
<#
.SYNOPSIS
Generate an OpenAPI/Swagger file using swashbuckle CLI tools. Requires your web project to use Swashbuckle.AspNetCore 5+
This function assumes that dotnet swagger tool is installed as a dotnet core local tool

.DESCRIPTION
Install and invoke invoke dotnet swagger global tool. See https://github.com/domaindrivendev/Swashbuckle.AspNetCore/#swashbuckleaspnetcorecli

.PARAMETER WebProjectFolderName
The folder name that contains the web project, e.g "MyApp.Web"

.PARAMETER StartupAssembly
The file name of the assembly that references swashbuckle. E.g. MyApp.Web.Dll

.PARAMETER OutputFilePath
The full path to the file where the swagger/OpenAPI json should be generated

.PARAMETER SwaggerDocName
The name of the swagger doc defined in your project startup. Usually 'v1'

	.PARAMETER IsOpenApi
	A bool switch to have swagger version V3(OpenApi) or V2. Defaults to False.

.EXAMPLE
Invoke-DotnetSwagger -WebProjectFolderName "Web" -StartupAssembly "MyApp.Web.dll" -OutputFilePath "$ReleaseDir\..\infrastructure\myapp-api-swagger.json"

#>
Function Invoke-DotnetSwagger(
	[string] $WebProjectFolderName,
	[string] $StartupAssembly = "$WebProjectFolderName.dll",
	[string] $OutputFilePath,
	[string] $SwaggerDocName = "v1",
	[switch] $IsOpenApi = $false
) {
	Invoke-BuildTask -Name "dotnet swagger" -command {
		try { 
			Push-Location $SolutionFolder
		
			Initialize-DotnetTool "swashbuckle.aspnetcore.cli"

			$binFolder = "$SolutionFolder\$WebProjectFolderName\bin"
			if (-Not(Test-Path $binFolder)) {
				throw "Cannot find bin folder for project to generate swagger file - $binFolder"
			}

			$WebDllPath = ( GetMostRecentFileMatchingPath -FilePathPattern $StartupAssembly -Project $binFolder -ExcludePattern "\\ref\\" )
			$jsonFile = Force-ResolvePath $OutputFilePath

			if ($IsOpenApi) {
				Invoke-Exec { dotnet swagger tofile --output $jsonFile $($WebDllPath.FullName) $SwaggerDocName }
			}
			else {
				Invoke-Exec { dotnet swagger tofile --serializeasv2  --output $jsonFile $($WebDllPath.FullName) $SwaggerDocName }
			}
			  	
			if (-Not(Test-Path $jsonFile)) {
				throw "Cannot find generated swagger file - dotnet swagger probably failed - $jsonFile"
			}

			return $jsonFile
		
		}
		finally {
			Pop-Location
		}
	}
}

<#
.SYNOPSIS
	Calls Resolve-Path but works for files that don't exist.
.REMARKS
	From http://devhawk.net/blog/2010/1/22/fixing-powershells-busted-resolve-path-cmdlet
#>
function Force-ResolvePath {
	
	param (
		[string] $FileName
	)
	     

	$FileName = Resolve-Path $FileName -ErrorAction SilentlyContinue `
		-ErrorVariable _frperror
	if (-not($FileName)) {
		$FileName = $_frperror[0].TargetObject
	}
	return $FileName
}
	
<#
.SYNOPSIS
Gets the build number from the environment

.NOTES
Gets the build number based on the env variables and a version file.
Without VERSION.txt file, the build number will come from BUILD_NUMBER env variable, and default to 1.0.0.0
With VERSION.txt, the build number will come from the first line of the file, e.g. 2.8.0.%BUILD_COUNTER%. 
Any Environment variables in %variable% syntax in the file such as BUILD_COUNTER will be substituted automatically
This function will set the ENV variables BUILD_COUNTER and BUILD_NUMBER if they are not already available.
Returns the caluculated build number.
#>
function Get-BuildNumber {
	$env:BUILD_COUNTER = if (Test-Path env:\BUILD_COUNTER) { $env:BUILD_COUNTER } else { "0" } 
	$env:BUILD_NUMBER = if (Test-Path env:\BUILD_NUMBER) { $env:BUILD_NUMBER } Else { "1.0.0.0" }
	$versionFileValue = if (Test-Path $SolutionFolder\VERSION.txt) { 
		Get-content $SolutionFolder\VERSION.txt -First 1 | ForEach-Object { [Environment]::ExpandEnvironmentVariables($_) }
	} 
	else { "" }

	if ($versionFileValue) { 
		$env:BUILD_NUMBER = $versionFileValue
		if ($IsTeamcity) { Write-Host "##teamcity[buildNumber '$env:BUILD_NUMBER']" }
		InfoMessage "Building as version $env:BUILD_NUMBER"
	}
	return $env:BUILD_NUMBER
}
	
 
<# Computed and other variables #> 
# force this module to execute as if it were in the location of the entry script
$PSScriptRoot = Get-ScriptDirectory
$SolutionFolder = (Get-Item -Path $PSScriptRoot -Verbose).FullName
$ProjName = try { (GetMostRecentFileMatchingPath "*.sln" -Project $SolutionFolder).Name.Replace(".sln", "") } catch {}
$PathToSln = Join-Path $SolutionFolder "$ProjName.sln"
$ReleaseDir = Join-Path $SolutionFolder $OutputDirectory
$_ = New-Item -path $ReleaseDir -type directory -force -ErrorAction silentlycontinue
$ReleaseDir = Resolve-Path $ReleaseDir
$LogFile = Join-Path $ReleaseDir "$ProjName-Build.log"
$NunitTestOutput = "$ReleaseDir\TestResult.xml"
$Stopwatch = [Diagnostics.Stopwatch]::StartNew()
$IsTeamcity = If (Test-Path env:\TEAMCITY_VERSION) { $true } Else { $false }
$BuildNumber = if (Test-Path env:\BUILD_NUMBER) { $env:BUILD_NUMBER } Else { "1.0.0.0" }
$BranchName = If(Test-Path env:\BRANCH_NAME ) { $env:BRANCH_NAME } else { & git rev-parse --abbrev-ref HEAD }
$IsMasterBuild = [bool] ($BranchName -eq "master" -Or $BranchName -eq "main")
$NugetVersionSuffix = If ($IsMasterBuild) { "" } else { "-prerelease" } #bug in dotnet tools xplat means you need to lowercase this
$CoverageDataFolder = Join-Path (Resolve-Path $ReleaseDir) "\Coverage"
$buildTasks = [System.Collections.ArrayList] @()
$testToolsInstalled = $false

Write-Host "Info: Running under Powershell version: " + $PSVersionTable.PSVersion
Write-Host "Info: Current Location is $SolutionFolder"
if (-not(Test-Path $PathToSln)) { WarnMessage "Unable to locate sln at $PathToSln" }

Export-ModuleMember -Variable OutputDirectory, ToolsPath, SolutionFolder, ProjName, PathToSln, ReleaseDir, IsTeamcity, BuildNumber, BranchName, IsMasterBuild, NugetVersionSuffix -Function InfoMessage, WarnMessage, GetMostRecentFileMatchingPath, GetFoldersContainingFilesThatMatch, Clean, Find-Nuget, Find-MsBuild, Restore, Build, Invoke-Build, Invoke-BuildCore, Invoke-PublishCore, Test-NUnit, Invoke-TestNUnit, Invoke-TestCore, Pack, New-ZipPackage, Install-Octo, ReportResults, Publish-TCArtifacts, Publish-ToOctopusGallery, Invoke-Clean, Invoke-CleanCore, Invoke-Exec, Invoke-PackCore, Publish-ToNugetGallery, Invoke-DotnetSwagger, Force-ResolvePath, Invoke-BuildTask